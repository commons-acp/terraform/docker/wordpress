resource "docker_container" "wordpress" {
  name  = var.container_name
  image = docker_image.wordpress.latest
  env = var.wordpress_env
  command = ["/usr/bin/import_config", join(", ", var.wordpress_env), var.wp_name]
  tty = false
  stdin_open = true

  ports {
    internal = 80
    ip = "127.0.0.1"
  }

  volumes {
    volume_name = docker_volume.shared_volume.name
    container_path = "/var/www/html"
    host_path = docker_volume.shared_volume.mountpoint
    read_only = false
  }

  networks_advanced {
    name = var.network_name
  }
}

resource "docker_volume" "shared_volume" {
  name = "${var.container_name}_volume"
}

resource "docker_image" "wordpress" {
  name = "registry.gitlab.com/commons-acp/docker-images/wordpress:1.9.0"
}
