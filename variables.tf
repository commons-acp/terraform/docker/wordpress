variable "container_name" {
  description = "container name"
}

variable "network_name" {
  description = "network name of the database container"
  default = "bridge"
}

variable "wordpress_env" {
  description = "database config"
  type = set(string)
}

variable "wp_name" {
  description = "name of the zip file that contains the wordpress configuration in the registry package of the infra-localhost project (without .zip)"
}


